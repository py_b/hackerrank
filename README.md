HackerRank
==========

Personal implementations for [hackerrank.com](https://www.hackerrank.com)
problems.

Languages used :

  - Haskell *
  - R

_* I'm learning Haskell, therefore solutions for this language are probably not
the most efficient._
