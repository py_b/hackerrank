solve <- function(numbers) {
  count <- table(factor(numbers, levels = 0:100))
  max(count[-1] + count[-length(count)])
}

numbers <- readLines("/dev/stdin", warn = FALSE)[-1]
numbers <- as.integer(strsplit(numbers, " ")[[1]])

cat(solve(numbers))
