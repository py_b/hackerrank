tests <- readLines("/dev/stdin", warn = FALSE)[-1]

res <-
  sapply(
    strsplit(tests, " "),
    function(x) {
      x <- as.double(x)
      (x[2] + x[3] - 2) %% x[1] + 1
    }
  )

writeLines(format(res, scientific = FALSE,  trim = TRUE))
