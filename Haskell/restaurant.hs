solve :: [Int] -> Int
solve [l, b] = (l * b) `div` d ^ 2
  where d = gcd l b

main = interact $
  unlines .
  map (show . solve . map read . words) .
  tail .
  lines
