solve :: Int -> [Int] -> Int -> String
solve k bill b
  | diff == 0 = "Bon Appetit"
  | otherwise = show diff
  where 
    actual = (sum bill - bill !! k) `div` 2
    diff   = b - actual

main = do
  [_, k] <- map read . words <$> getLine
  bill   <- map read . words <$> getLine
  b      <- read <$> getLine
  putStr $ solve k bill b
