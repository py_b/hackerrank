import Data.List (sort, nub)
import Data.List.Split (chunksOf)

solve :: [Int] -> [Int]
solve [n, a, b] = sort $ nub vals
  where 
    n' = n - 1
    vals = map (\k -> (n' - k) * a + k * b) [0..n']

main = do
  tests <- (chunksOf 3) . map read . tail . lines <$> getContents
  putStr $ unlines $ map (unwords . map show . solve) tests
