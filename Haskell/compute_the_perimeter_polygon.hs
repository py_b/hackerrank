main = interact $
  show .
  perimeter .
  closePolygon .
  map (listToPoint . map read . words) .
  tail .
  lines

data Point = Point {x :: Float, y :: Float} deriving (Eq, Show)
type Polygon = [Point]

listToPoint :: [Float] -> Point
listToPoint [x, y] = Point x y 

dist :: Point -> Point -> Float
dist a b = sqrt $ (x a - x b) ^ 2 + (y a - y b) ^ 2

closePolygon :: Polygon -> Polygon
closePolygon p
  | last p == head p  =  p
  | otherwise         =  p ++ [head p]

perimeter :: Polygon -> Float
perimeter [p1]       = 0
perimeter (p1:p2:ps) = dist p1 p2 + perimeter (p2:ps)
