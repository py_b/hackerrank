solve :: [Int] -> [Int]
solve [px, py, qx, qy] = [2 * qx - px, 2 * qy - py]

main = interact $
  unlines .
  map (unwords . map show . solve . map read . words) .
  tail .
  lines
