import Data.List (sort, group, nub)
import Data.List.Split (splitWhen)

isfun :: Ord a => [[a]] -> String
isfun x = if any (>1) images_nb then "NO" else "YES"
  where images_nb = map length $ group $ sort $ map head $ nub x

main = interact $
  unlines .
  map isfun .
  tail .
  splitWhen (\x -> length x == 1) .
  map words .
  tail .
  lines
