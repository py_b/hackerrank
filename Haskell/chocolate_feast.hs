main = interact $
  unlines .
  map (show . solve . (map read) . words) .
  tail .
  lines

type Bars = Int
type Wrap = Int

eatOrBuy :: Int -> (Bars, Wrap) -> (Bars, Wrap)
eatOrBuy m b_w
  | bars >  0 = (0           , bars + wrap)  -- eat
  | bars == 0 = (wrap `div` m, wrap `mod` m) -- buy
  where
    bars = fst b_w
    wrap = snd b_w

solve :: [Int] -> Int
solve [n, c, m] =
  sum $ map fst states
  where
    initState = (n `div` c, 0)
    notOver b_w = fst b_w > 0 || snd b_w `div` m > 0
    states = takeWhile notOver $ iterate (eatOrBuy m) initState

-- bonus : editorial solution using recursion
solve' :: [Int] -> Int
solve' [n, c, m] = n `div` c + getCnt (n `div` c)
  where
    getCnt :: Int -> Int
    getCnt wrappers
      | wrappers < m = 0
      | otherwise = wdm + getCnt (wdm + wrappers `rem` m)
      where wdm = wrappers `div` m
