solve n = foldl nextheight 1 [0..n-1]
  where nextheight height cycle = if even cycle then height * 2 else height + 1

main = interact $ unlines . map (show . solve . read) . tail . lines
