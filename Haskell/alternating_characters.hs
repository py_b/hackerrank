import Data.List

solve :: String -> Int
solve = sum . map (\x -> length x - 1) . group

main = interact $ unlines . map (show . solve) . tail . words
