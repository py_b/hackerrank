progDay :: Int -> Bool -> String
progDay year leapYear 
  | leapYear     = "12.09." ++ show year
  | not leapYear = "13.09." ++ show year

progDayJul :: Int -> String
progDayJul year = progDay year (year `mod` 4 == 0)

progDayGre :: Int -> String
progDayGre year = progDay year (leapC1 || leapC2)
  where
    leapC1 = year `mod` 4 == 0 && year `mod` 100 /= 0 
    leapC2 = year `mod` 400 == 0

solve :: Int -> String
solve year
  | year <  1918 = progDayJul year
  | year == 1918 = "26.09.1918"
  | year >  1918 = progDayGre year

main :: IO ()
main = interact $ solve . read
