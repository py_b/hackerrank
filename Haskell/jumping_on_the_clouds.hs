import Data.List

solve x = jump0 + jump1
  where grp01 = group x
        zeros = filter (any (=="0")) grp01
        ones  = filter (any (=="1")) grp01
        jump0 = sum $ map (flip div 2 . length) zeros
        jump1 = length(ones)

main = interact $ show . solve . tail . words
