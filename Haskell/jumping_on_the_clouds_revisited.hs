import Data.List.Split

solve :: [Int] -> Int
solve (k:clouds) = 100 - jumps - 2 * malus
  where n = length clouds
        full_clouds = concat $ replicate (lcm n k `div` n) clouds
        clouds_k = splitEvery k full_clouds
        jumps = length clouds_k
        malus = sum $ map head clouds_k

main = interact $ show . solve . map read . tail . words
