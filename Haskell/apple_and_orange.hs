solve :: [Int] -> [Int]
solve (s:t:a:b:m:_:fruits) = [ah, bh]
  where
    apples = take m fruits
    oranges = drop m fruits
    inhouse = filter (\x -> x >= s && x <= t)
    ah = length $ inhouse $ map (+a) apples
    bh = length $ inhouse $ map (+b) oranges

main = interact $ unlines . map show . solve . map read . words
