import Data.List

solve :: [Int] -> Int
solve x = head $ head $ filter ((==biggest) . length) grps
  where grps    = group $ sort x
        biggest = maximum $ map length grps

main = interact $ show . solve . map read . tail . words
