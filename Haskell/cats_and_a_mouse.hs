solve :: [Int] -> String
solve [catA, catB, mouseC]
  | dAC == dBC = "Mouse C"
  | dAC  < dBC = "Cat A"
  | dAC  > dBC = "Cat B"
  where dAC = abs (catA - mouseC)
        dBC = abs (catB - mouseC)

main = interact $
  unlines .
  map (solve . map read . words) .
  tail .
  lines
