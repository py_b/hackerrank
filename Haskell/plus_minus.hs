import Data.List

round6 x = (fromInteger $ round $ x * (10^6)) / (10.0^^6)

solve :: [Int] -> [Float]
solve x = map round6 $ map (/n) $ map fromIntegral nb_all
  where nb = map (`compare` 0) x
        nb_all = map (\o -> length $ filter (==o) nb) [GT, LT, EQ]
        n = fromIntegral $ length x

main = interact $ unlines . map show . solve . map read . tail . words
