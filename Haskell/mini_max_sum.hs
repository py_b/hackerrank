import Data.List

solve :: [Int] -> [Int]
solve x = let sx = sort x in map sum [init sx, tail sx]  

main = interact $ unwords . map show . solve . map read . words
