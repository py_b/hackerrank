solve :: [Int] -> Int
solve [m, n] = m * n - 1

main = interact $ show . solve . map read . words
