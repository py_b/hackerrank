solve :: Int -> [String] -> Int -> String
solve k array pos = rot_arr !! pos
  where m = length array - k `mod` length array
        rot_arr = drop m array ++ take m array
  
main = do
  [_, k, _] <- map read . words <$> getLine
  arr       <- words <$> getLine
  pos       <- map read . lines <$> getContents
  putStrLn $ unlines $ map (solve k arr) pos
