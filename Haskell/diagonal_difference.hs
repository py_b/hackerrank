import Data.List

solve :: [Int] -> Int
solve m = abs $ sum diag1 - sum diag2
  where
    n = round $ sqrt $ fromIntegral $ length m
    idiag1 = map (\x -> (n+1) * x) [0..n-1]
    idiag2 = map (\x -> (n-1) * x) [1..n]
    diag1 = map (\i -> m !! i) idiag1
    diag2 = map (\i -> m !! i) idiag2

main = interact $ show . solve . map read . tail . words
