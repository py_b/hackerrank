import Data.List.Split (splitEvery)

filterElem :: [a] -> [Int] -> [a]
filterElem x indxs = map snd $ filter (\t -> fst t `elem` indxs) $ zip [0..] x 

-- compute all hourglasses locations in the matrix
hgTopLeft = [ 0, 1, 2, 3,
              6, 7, 8, 9,
             12,13,14,15,
             18,19,20,21]
hgOffsets = [0,  1, 2,
                 7,
             12,13,14]
hgAll = splitEvery 7 $ (+) <$> hgTopLeft <*> hgOffsets

solve :: [Int] -> Int
solve mat = maximum $ map sum hgVals
  where hgVals = map (filterElem mat) hgAll

main = interact $ show . solve . map read . words
