import Data.List.Split (chunksOf)

solve :: [Int] -> [Int]
solve [n, k]
  | k == 0               = [1..n]
  | n `mod` (2 * k) /= 0 = [-1]
  | otherwise            = gp2k >>= concat . reverse . chunksOf k
  where gp2k = chunksOf (2 * k) [1..n]

main = interact $
  unlines .
  map (unwords . map show . solve . map read . words) .
  tail .
  lines
