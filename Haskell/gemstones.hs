import Data.List
import Data.Function

-- All (potential) gemstones are in the rock with most mineral
solve :: [String] -> Int
solve x = length gemstones
  where
    (big:smalls) = sortBy ((flip compare) `on` length) $ map nub x
    gemstones = filter (\m -> all (elem m) smalls) big

main = interact $ show . solve . tail . lines
