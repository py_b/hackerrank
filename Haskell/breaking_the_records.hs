import Data.List

record :: Ord a => (a -> a -> a) -> ([a] -> Int)
record optimum = length . tail . nub . scanl1 optimum

solve :: [Int] -> [Int]
solve scores = [record max scores, record min scores]

main = interact $
  unwords .
  map show .
  solve .
  map read .
  tail . 
  words
