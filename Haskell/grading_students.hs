solve x
  | x >= 38 && next_m5 - x < 3  = next_m5
  | otherwise                   = x
  where next_m5 = x + 5 - x `mod` 5

main = interact $ unlines . map (show . solve . read) . tail . lines
