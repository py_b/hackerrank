import Data.List

solve :: (Ord a) => [a] -> Int
solve x = length $ concat $ rest x
  where rest = init .
               sortBy (\x y -> length x `compare` length y) .
               group .
               sort

main = interact $ show . solve . tail . words
