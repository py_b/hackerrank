solve :: [Int] -> Int
solve [a, b] = floor sqb - ceiling sqa + 1
  where sqa = sqrt $ fromIntegral a
        sqb = sqrt $ fromIntegral b
        
main = interact $
  unlines .
  map (show . solve . map read . words) .
  tail .
  lines
