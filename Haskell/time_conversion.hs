import Data.List.Split

hourConv hh ap
  | read hh == 12 && ap == "A" = "00"
  | read hh <= 11 && ap == "P" = show $ read hh + 12
  | otherwise                  = hh

solve :: String -> String
solve x = hh1 ++ ":" ++ parts !! 2 ++ ":" ++ parts !! 4
  where
    parts = split (oneOf ":PA") x
    hh1 = hourConv (parts !! 0) (parts !! 5)
    
main = interact $ solve
