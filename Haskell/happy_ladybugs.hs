-- *If there is at least one empty cell*, we can rearrange the ladybugs 
-- in any order. Why ? You can move any LB to the cell you want : just
-- free the cell and place the LB on it. Repeat this process until
-- done.
-- If this condition is met, we just have to check there is no lone
-- ladybug. 

import Data.List
import Data.List.Split

ladyGrp :: String -> [String]
ladyGrp = filter ((/='_') . head) . group

solve :: String -> String
solve board
  | all (>1) $ map length grps = "YES" --already happy
  | nEmpty > 0 && smallest > 1 = "YES" --can rearrange
  | otherwise                  = "NO"
  where
    grps     = ladyGrp board
    nEmpty   = length $ filter (=='_') board
    smallest = minimum $ map length $ ladyGrp $ sort board

main = interact $
  unlines .
  map (solve . last) .
  (chunksOf 2) .
  tail .
  lines
