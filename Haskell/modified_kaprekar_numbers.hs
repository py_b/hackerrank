isKap :: Int -> Bool
isKap n =
  n == left + right
  where
    sqn = show $ n ^ 2
    nleft = length sqn - length (show n)
    right = read $ drop nleft sqn
    left  = if nleft > 0 then read $ take nleft sqn else 0

solve :: [Int] -> String
solve [p, q]
  | length kaps > 0 = unwords $ map show kaps
  | otherwise       = "INVALID RANGE"
  where kaps = filter isKap $ [p..q]

main = interact $ solve . map read . words
