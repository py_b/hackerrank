solve :: String -> String -> String
solve p q = concat $ zipWith (\x y -> [x, y]) p q

------ with recursion
-- solve [] [] = []
-- solve (x:xs) (y:ys) = x:y: solve xs ys

main = do
  p <- getLine
  q <- getLine
  putStrLn $ solve p q