allMagic :: [[Int]]
allMagic = [
    [8, 1, 6, 3, 5, 7, 4, 9, 2]
  , [6, 1, 8, 7, 5, 3, 2, 9, 4]
  , [4, 3, 8, 9, 5, 1, 2, 7, 6]
  , [2, 7, 6, 9, 5, 1, 4, 3, 8]
  , [2, 9, 4, 7, 5, 3, 6, 1, 8]
  , [4, 9, 2, 3, 5, 7, 8, 1, 6]
  , [6, 7, 2, 1, 5, 9, 8, 3, 4]
  , [8, 3, 4, 1, 5, 9, 6, 7, 2]
  ]

solve :: [Int] -> Int
solve square = minimum $ map (cost square) allMagic
  where
    cost :: [Int] -> [Int] -> Int
    cost s m = sum $ zipWith (\x y -> abs $ x - y) s m

main = interact $ show . solve . map read . words
