listPart :: Int -> Int -> [a] -> [a]
listPart start end = take (end - start + 1) . drop (start)

solve :: [Int] -> [Int] -> Int
solve w [start, end] = minimum $ listPart start end w

main = do
  _ <- getLine
  widths <- map read . words <$> getLine
  indexes <- map (map read . words) . lines <$> getContents
  putStrLn $ unlines $ map (show . (solve widths)) indexes
