import Data.List

ud :: Char -> Int
ud 'U' = 1
ud 'D' = -1

solve :: String -> Int
solve =
  length .
  filter (all (<0)) .
  groupBy (\x y -> x * y > 0) .
  scanl (+) 0 .
  map ud

main = interact $ show . solve . head . tail . words
