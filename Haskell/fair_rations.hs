import Data.List (findIndices)
import Data.List.Split (chunksOf)

-- if odd number of odds => impossible
-- otherwise, it takes (n + 1) * 2 loaves to "evenize" a
-- range of n even numbers surrounded by 2 odd numbers

solve :: [Int] -> String
solve x
  | odd nbOdd = "NO"
  | otherwise = show $ loaves x
  where
    nbOdd  = length $ filter odd x
    loaves = sum .
             map (\[x, y] -> (y - x) * 2) . 
             chunksOf 2 .
             findIndices odd

main = interact $ solve . map read . tail . words
