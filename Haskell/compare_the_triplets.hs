solve :: [Int] -> [Int] -> [Int]
solve a b = [n GT, n LT]
  where comp = zipWith compare a b
        n ord = length $ filter (==ord) comp

main :: IO ()
main = do
  triplets <- sequence [getLine, getLine]
  let [a, b] = map (map read . words) triplets :: [[Int]]
  putStrLn $ unwords $ map show $ solve a b
