solve :: [Int] -> Int
solve = foldl1 (\m n -> m * n `mod` 1234567)

main = interact $
  unlines .
  map (show . solve . map read) .
  filter ((>1) . length) .
  map words .
  lines
