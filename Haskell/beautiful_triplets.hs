nbtrip_head :: Int -> [Int] -> Int
nbtrip_head d (x:xs) =
  product $ map length cand
  where
    cand = [filter (\k -> k == x + d)     xs
           ,filter (\k -> k == x + 2 * d) xs
           ]

solve :: [Int] -> Int
solve (d:vals) = sum $ map (nbtrip_head d) $ tails2
  where
    k = length vals
    tails2 = map (\n -> drop n vals) [0..k-3]
  
main = interact $ show . solve . map read . tail . words
