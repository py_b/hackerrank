import Data.List

solve :: [Int] -> Int
solve = sum . map (flip div 2 . length) . group . sort

main = interact $ show . solve . map read . tail . words
