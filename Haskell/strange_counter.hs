-- brute force...
-- compute indexes where solve == 1
index1 = takeWhile (<=10^13) $ scanl (+) 0 $ iterate (*2) 3

solve :: Int -> Int
solve t = next - t + 1
  where
    prec = length $ takeWhile (<t) $ index1
    next = index1 !! prec

main = interact $ show . solve . read
