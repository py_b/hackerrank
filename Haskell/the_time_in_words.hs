import Data.List
import Data.Maybe

corresp = zip [1..] [
  "one", "two", "three", "four", "five",
  "six", "seven", "eight", "nine", "ten",
  "eleven", "twelve", "thirteen", "fourteen","fifteen",
  "sixteen", "seventeen", "eighteen", "nineteen","twenty",
  "twenty one", "twenty two", "twenty three", "twenty four",
  "twenty five", "twenty six", "twenty seven", "twenty eight",
  "twenty nine"
  ]

solve :: [Int] -> String
solve [h, m]
  | m == 0          = hs ++ " o' clock"
  | m `mod` 15 == 0 = hq ++ pt ++ hs
  | otherwise       = ms ++ min ++ pt ++ hs
  where
    hq = if m == 30 then "half" else "quarter"
    pt = if m <= 30 then " past " else " to "
    m' = if m <= 30 then m else 60 - m
    h' = if m <= 30 then h else h + 1
    min = if m' == 1 then " minute" else " minutes"
    ms = fromJust $ lookup m' corresp
    hs = fromJust $ lookup h' corresp

main = interact $ solve . map read . lines
