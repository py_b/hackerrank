rotLeft n xs = drop n xs ++ take n xs

main = do
  line1 <- getLine
  let n = read $ last $ words line1
  line2 <- getLine
  putStrLn $ unwords $ rotLeft n (words line2)
