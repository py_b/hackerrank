solve [r, c]  = x + y + z
  where x = 10 * ((r - 1) `div` 2)
        y = 2 * c - 2
        z = (r - 1) `mod` 2

main = interact $ show . solve . map read . words
