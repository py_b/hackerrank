solve :: [Int] -> Int
solve = product . map ((`div` 2) . (+ 1))

main = interact $ show . solve . map read . words
