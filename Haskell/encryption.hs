import Data.List
import Data.List.Split

solve :: String -> String
solve x = unwords $ transpose $ chunksOf n x
  where n = ceiling $ sqrt $ fromIntegral $ length x

main = interact $ solve
