import Data.List

compress1 :: (Char, Int) -> String
compress1 (c, n)
  | n == 1    = [c]
  | otherwise = [c] ++ show n

compress :: String -> String
compress x = concat $ map compress1 count
  where grpx = group x
        count = zip (map head grpx) (map length grpx)

main = interact $ compress
