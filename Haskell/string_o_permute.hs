solve :: String -> String
solve [] = []
solve [a] = [a]
solve (a:b:xs) = b:a:solve xs

main = interact $ unlines . map solve . tail . lines