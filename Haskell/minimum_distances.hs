import qualified Data.Map as Map
import Data.Maybe
import Data.List

solve :: [Int] -> Int
solve x = fromMaybe (-1) $ listToMaybe $ sort dist
  where
    list_indices = (\x y -> (x, [y]))
    dic = Map.fromListWith (++) $ zipWith list_indices x [0..]
    dic2 = Map.toList $ Map.filter ((==2) . length) dic
    dist = map (\x -> abs(head (snd x) - last (snd x))) dic2

main = interact $ show . solve . map read . tail . words
