import Data.List

reducible :: Ord a => [a] -> Bool
reducible = any (>1) . map length . group

reduce1 :: Eq a => [a] -> [a]
reduce1 []  = []
reduce1 [c] = [c]
reduce1 (a:b:cs)
  | a == b    = cs
  | otherwise = a:reduce1(b:cs)

reduce :: (Ord a, Eq a) => [a] -> [a]
reduce = reduce1 .
         last .
         takeWhile reducible .
         iterate reduce1

solve :: String -> String
solve s
  | redS == "" = "Empty String"
  | otherwise  = redS
  where redS = reduce s

main = interact $ solve
