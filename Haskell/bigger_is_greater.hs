import Data.Maybe

main = interact $
  unlines .
  map (fromMaybe "no answer" . solve) .
  tail .
  lines

initNonDec :: (Ord a) => [a] -> [a]
initNonDec (x:y:zs) = if x <= y then x : initNonDec (y:zs) else [x]
initNonDec emptyOrSingleton = emptyOrSingleton

newSuff :: (Ord a) => [a] -> a -> [a]
newSuff x pivot = 
  [b1] ++ smalls ++ [pivot] ++ bigs
  where
    smalls    = filter (<=  pivot) x
    (b1:bigs) = filter (>  pivot) x

solve :: (Ord a) => [a] -> Maybe [a]
solve x
  | idPivot < 0 = Nothing
  | otherwise   = Just $ (take idPivot x) ++ (newSuff revSuff $ x !! idPivot)
  where
    revSuff = initNonDec $ reverse x
    idPivot = length x - length revSuff - 1
