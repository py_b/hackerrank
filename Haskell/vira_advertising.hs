solve :: Int -> Int
solve n = sum $ take n $ iterate next 2
  where next = (`div` 2) . (*3)

main = interact $ show . solve . read
