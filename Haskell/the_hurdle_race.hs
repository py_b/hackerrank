solve :: [Int] -> Int
solve (k:hs)
  | diff < 0  = 0
  | otherwise = diff
  where diff = maximum hs - k

main = interact $ show . solve . map read . tail . words
