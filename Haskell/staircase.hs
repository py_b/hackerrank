step n m = replicate (n - m) ' ' ++ replicate m '#'
staircase n = map (step n) [1..n]

main = interact $ unlines . staircase . read
