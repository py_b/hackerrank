main = do getLine
          a <- map read . words <$> getLine
          b <- map read . words <$> getLine
          putStr $ show $ solve a b

solve :: [Int] -> [Int] -> Int
solve a b = length $ between
  where
   lcmA = foldl1 lcm a
   gcdB = foldl1 gcd b
   divGcdB k = gcdB `mod` k == 0
   between = filter divGcdB $ takeWhile (<=gcdB) $ map (*lcmA) [1..]
