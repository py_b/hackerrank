cut1 :: [Int] -> [Int]
cut1 sticks =
  map (subtract m) $ filter (>m) sticks
  where m = minimum sticks

solve :: [Int] -> [Int]
solve = map length . cut
  where cut = takeWhile ((>0) . length) . iterate cut1

main = interact $
  unlines .
  map show .
  solve .
  map read .
  tail .
  words
