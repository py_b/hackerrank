import Data.List
import Data.Maybe

solve :: [Int] -> String -> Int
solve heights word = length word * max_height
  where
    letter_heights = zip ['a'..'z'] heights
    word_heights = map (flip lookup letter_heights) word
    max_height = maximum $ map (fromMaybe (-1)) word_heights

main = do
  heights0 <- getLine
  let heights = map read $ words $ heights0
  word <- getLine
  putStrLn $ show $ solve heights word
