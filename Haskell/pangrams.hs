import Data.List
import Data.Char

-- naive short version
pangram' = (==['a'..'z']) . nub . sort . map toLower . filter (/=' ')

-- maybe faster version, if [a-z] in the beginning of big string ?
pangram :: String -> Bool
pangram s =
  cumNub == ['a'..'z']
  where
    lowerS = map toLower $ filter (/=' ') s
    cumLetter xs x = if xs == ['a'..'z']
                     then "ALL" 
                     else sort $ nub $ x:xs
    cumNub = last $
             takeWhile (/="ALL") $
             scanl cumLetter "" lowerS

main = do
  s <- getLine
  putStrLn $ if pangram s then "pangram" else "not pangram"
