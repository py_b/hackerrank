import Data.List
import Control.Monad

solve :: [Int] -> Int -> Int -> Int
solve xs d m = length $ filter (==d) sum_contig_m
  where
    debuts_m = filter ((>= m) . length) $ tails xs
    sum_contig_m = map (sum . take m) debuts_m

main = do
  getLine
  xs     <- liftM (map read . words) getLine
  [m, d] <- liftM (map read . words) getLine
  putStrLn $ show $ solve xs m d
