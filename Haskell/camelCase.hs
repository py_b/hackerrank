import Data.Char

solve :: String -> Int
solve s = 1 + (length $ filter isUpper s)

main = interact $ show . solve
