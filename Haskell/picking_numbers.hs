import Data.List

-- filtre liste x selon présence elem x dans xf
list_filter :: [Int] -> [Int] -> [Int]
list_filter x xf = filter (flip elem $ xf) x

solve :: [Int] -> Int
solve input = maximum $ map (length . (list_filter input)) cand
  where 
    unique = nub $ sort input
    a = case unique of [x] -> [x]
                       xs -> init xs
    b = case unique of [x] -> [x]
                       xs -> tail xs
    pair_cand = filter (\(m,n) -> n - m <= 1) $ zip a b
    cand0 = map (\(m,n) -> [m,n]) pair_cand
    cand = cand0 ++ map (:[]) unique

main = interact $ show . solve . map read . tail . words
