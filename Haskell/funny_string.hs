import Data.Char (ord)

diff :: Num a => [a] -> [a]
diff [x1, x2]   = [abs $ x1 - x2]
diff (x1:x2:xs) = (abs $ x1 - x2):(diff (x2:xs))

solve :: String -> String
solve s =
  if d == reverse d then "Funny"
                    else "Not Funny"
  where d = diff $ map ord s

main = interact $ unlines . map solve . tail . lines
