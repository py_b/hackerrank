price :: Int -> Int -> Int -> Int -> Int
price p d m n = max (p - d * n) m

solve :: Int -> Int -> Int -> Int -> Int
solve p d m s =
  length $ takeWhile (<=s) $ scanl1 (+) priceList
  where priceList = map (price p d m) [0..]

main :: IO ()
main = do
  [p, d, m, s] <- map read . words <$> getLine
  putStrLn $ show $ solve p d m s
