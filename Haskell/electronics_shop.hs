import Control.Monad

solve :: Int -> [Int] -> [Int] -> Int
solve b drives usbs =
  let
    comb_b = filter (<= b) $ (+) <$> drives <*> usbs
  in
    if length comb_b > 0 then maximum comb_b else -1

main = do
  [b,_,_] <- liftM (map read . words) getLine
  drives  <- liftM (map read . words) getLine
  usbs    <- liftM (map read . words) getLine
  putStrLn $ show $ solve b drives usbs
