import Data.List.Split

solve :: [Int] -> Int
solve [w, b, wc, bc, z] =
  let
    wc' = bc + z
    bc' = wc + z
  in
    w * min wc wc' + b * min bc bc'

main = interact $
  unlines .
  map (show . solve) .
  chunksOf 5 .
  map read .
  tail .
  words
