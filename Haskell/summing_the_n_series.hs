nmax = 10 ^ 9 + 7

nthmod :: Int -> Int
nthmod n = (2 * (n `mod` nmax) - 1) `mod` nmax

plus_mod :: Int -> Int -> Int -> Int
plus_mod k x y = (x `mod` k + y `mod` k) `mod` k

solve :: Int -> Int
solve n = foldl1 (plus_mod nmax) tn
  where tn = map nthmod [1..n]
    
main = interact $ unlines . map (show . solve . read)  . tail . lines

{- echoue pour

    10
    229137999
    344936985
    681519110
    494844394
    767088309
    307062702
    306074554
    555026606
    4762607
    231677104
	
	expected
	218194447
    788019571
    43914042
    559130432
    685508198
    299528290
    950527499
    211497519
    425277675
    142106856
-}
