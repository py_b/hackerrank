import Data.List (inits)

-- Given a start integer and a minimum string length,
-- generate the smallest beautiful numeric string
-- Example : beaufiful 6 9 == "91011"
beaufiful :: Int -> Int -> String
beaufiful len start =
  last $
  takeWhile ((<=len) . length) $
  scanl1 (++) $
  map show [start..]

-- Given a string, find the starting integer of
-- the beautiful string. If not beautiful : Nothing
beautyStart :: String -> Maybe Int
beautyStart s =
  let
    n = length s
    starts = map read $ tail $ inits $ take (n `div` 2) s
    beaufifuls = map (beaufiful n) starts
  in
    lookup s $ zip beaufifuls starts

solve :: String -> String
solve = maybe "NO" (\i -> "YES " ++ show i) . beautyStart

main = interact $ unlines . map solve . tail .lines
