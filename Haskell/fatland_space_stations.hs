import Data.List

solve :: [Int] -> Int
solve (n:_:s) = maximum (distFirst:distLast:interm)
  where
    s' = sort s
    distFirst = head s'
    distLast = n - 1 - last s'
    interm = zipWith (\a b -> (b - a) `div` 2) (init s') (tail s')

main = interact $ show . solve . map read . words
