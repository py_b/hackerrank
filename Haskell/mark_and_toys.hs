import Data.List

solve :: [Int] -> Int
solve (budget:prices) = length $ takeWhile (<=budget) $ scanl1 (+) inbudget
  where inbudget = sort $ filter (<=budget) prices
  --probably shorter with this filter

main = interact $ show . solve . map read . tail . words
