import Data.Char (digitToInt)

solve :: String -> Int
solve str_i = length divisors
  where
    digits = map digitToInt str_i
    i = read str_i
    divisors = filter (\k -> k /= 0 && i `mod` k == 0) digits

main = interact $ unlines . map (show . solve) . tail . lines
