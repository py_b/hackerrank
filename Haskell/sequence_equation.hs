import Data.List
import Data.Function (on)

-- indexes to take in order to have the list sorted (from 1)
order :: Ord a => [a] -> [Int]
order x = map fst $ sortBy (compare `on` snd) pairx 
  where pairx = zip [1..length x] x

solve :: [Int] -> [Int]
solve x = map (+1) $ concatMap (\y -> elemIndices y x) $ order x

main = interact $
  unlines .
  map show .
  solve .
  map read .
  tail . 
  words
