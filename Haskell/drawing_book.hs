solve :: [Int] -> Int
solve [n, p] = min left right
  where left  = p `div` 2
        right = n `div` 2 - left

main = interact $ show . solve . map read . lines
