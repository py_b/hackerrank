import Data.List.Split

solve :: [Int] -> Int
solve (k:arr) = length $ filter isSpecial num_pages
  where
    isSpecial (page, problems) = page `elem` problems
    pages = concat $ map (\n -> chunksOf k [1..n]) arr
    num_pages = zip [1..] pages

main = interact $ show . solve . map read . tail . words
